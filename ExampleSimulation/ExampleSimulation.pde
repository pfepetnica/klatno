// Moram imati klasu koja prestavlja paket upravljanja pod nazivom Actuation
class Actuation
{
  float Sila = 0;
}


class Measurement
{
  float mX = 0;
}


class MojaSimulacija extends Simulation
{
  float nekaVrednost = 0;
  float dt, st;
  float x2, x, y, x1, y1, g, l;
  float tt, w, a;
  float p, ai, v1;
  float temp1, temp2;
  float er, gain, er1, ei, erp;
  MojaSimulacija ()
  {
    // Podesim frekvenciju update-jta simulacije da bude 10 puta veca od
    // frame rate-jta (30 fps)
    SetUpdateMul(10);
    x1 = width/2; y1 = height/2;
    l = 2.0;
    g = 9.81;
    p = 0.1;
   
    st = 0;
    gain = 0.2;
    erp = 0;
    
    x = x1;
    y = y1;
    //temp1 = mouseX;
    //temp2 = temp1;
    tt = 20;
   
    x2 = 6*width/10;
    // Podesim periodu kontrolera tako da bude 5 puta manja od perioda simulacije
    SetControlMul(5);
    
    dt = GetUpdatePeriod();
    print(dt);
  }


  void Update(Actuation input)
  {
   st += dt; 
  //print(tt);
   
   er = -tt * gain;
   ei = (er - erp) / dt;
   erp = er;
   ai = ei + 2 * er;
   
   
   //ai = input.Sila / 20; */
    
   a = (-g * sin(tt) + ai * cos(tt))/l;
   //print(a);
   w += a * dt;
   tt += w * dt;
    
    v1 -= (ai * dt);
    x1 += v1 * dt;
   
  }

  // Napravim merenja (merenja se uvek desava pre izvrsavanja kontrolera)
  Measurement Measure()
  {
    Measurement m = new Measurement();
    m.mX = 0;
    return m;
  }

  // Izracunam novo upravljanje na osnovu merenja
  Actuation Control(Measurement m)
  {
    Actuation o = new Actuation();
    o.Sila = -20 * sin(sqrt(g/l)*st);
    if  (x1 > x2) 
        o.Sila = 0;
    return o;
  }

  // Vizualizujem rezultat simulacije (frame rate je 30 fps)
  void Visualize()
  {
    background(230);
    stroke(255, 0, 0);
    y = y1 + 100 * cos(tt);
    x = x1 + 100 * sin(tt);
    
    line(x1, y1, x, y);
    ellipse(x, y, 25, 25);
    
    stroke(0, 255, 0);
    line(width/4, height/2, 3*width/4, height/2);
    stroke(0, 0, 2);
    line(width/2, 0, width/2, height);
  }
}

// Moram implementirati funkciju koja inicijalizuje simulaciju
// i u njoj postaviti globalnu promenljivu sim na instancu moje simulacije
void InitSimulation()
{
  surface.setSize(640, 360);
  stroke(255);
  
  sim = new MojaSimulacija();
}